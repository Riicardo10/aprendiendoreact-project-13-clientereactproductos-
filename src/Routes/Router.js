import React, { Component }             from 'react';
import {BrowserRouter, Route, Switch}   from 'react-router-dom';

import Header           from '../components/Header/Header';
import Contacto         from '../components/Contacto/Contacto';
import Navegacion       from '../components/Navegacion/Navegacion';
import Nosotros         from '../components/Nosotros/Nosotros';
import Error404         from '../components/Error/Error404';
import Productos        from '../components/Productos/Productos';
import DetalleProducto  from '../components/Productos/DetalleProducto';

import productos     from '../data/data.json';

class Router extends Component {
    
    data = [];

    state = {
        productos: [],
        filtro: ''
    }

    componentWillMount() {
        this.setState( {productos} );
    }
    
    buscarProducto = (filtro) => {
        this.setState( {filtro} );
    }

    render() {

        let productos_all = [...this.state.productos];
        let filtro    = (this.state.filtro).toLowerCase();
        this.data = productos_all.filter( producto => {
            return producto.nombre.toLowerCase().indexOf( filtro ) !== -1;
        } );
        return (
            <BrowserRouter>
                <div className="contenedor">
                    <Header />
                    <Navegacion />
                    <Switch>
                        <Route exact path='/nosotros'       component={Nosotros}></Route>
                        <Route exact path='/'               render={ this.showProductos }></Route>
                        <Route exact path='/productos'      render={ this.showProductos }></Route>
                        <Route exact path='/producto/:id'   render={ this.showProducto }></Route>
                        <Route exact path='/contacto'       component={Contacto}></Route>
                        <Route component={Error404}></Route>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }

    showProductos   = () => <Productos productos={this.data} buscarProducto={this.buscarProducto} />
    showProducto    = (props) => {
        // let id = props.location.pathname.replace( '/producto/', '' );
        let id = props.match.params.id;
        return (
            <DetalleProducto producto={this.state.productos[id]} />
        );
    }
}

export default Router;