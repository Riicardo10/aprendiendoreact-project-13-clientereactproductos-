import React, { Component } from 'react';
import '../../styles/buscador.css';

class Buscador extends Component {

    buscar = (e) => {
        e.preventDefault();
        const filtro = e.target.value;
        this.props.buscarProducto( filtro );
    }

    render() {
        return (
            <form className='buscador'>
                <input type='text' placeholder='Ingresa producto a buscar...' onChange={this.buscar} />
            </form>
        );
    }
}

export default Buscador;