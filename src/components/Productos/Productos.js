import React, { Component } from 'react';
import Producto             from './Producto' ;
import '../../styles/productos.css';
import Buscador from '../Buscador/Buscador';

class Productos extends Component {
    render() {
        return (
            <div className='productos'>
                <h2> Nuestros productos </h2>
                <Buscador buscarProducto={this.props.buscarProducto} />
                <ul className='lista-productos'>
                    { Object.keys( this.props.productos ).map( producto => {
                        return <Producto key={producto} producto={this.props.productos[producto]} />;
                    } ) }
                </ul>
            </div>
        );
    }
}

export default Productos;