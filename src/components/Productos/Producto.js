import React from 'react';
import {Link}   from 'react-router-dom';

const Producto = ( props ) => {
    const {id, nombre, precio, imagen, descripcion} = props.producto;
    return (
        <li>
            <img src={'img/' + imagen + '.png'} alt={nombre} title={nombre}/>
            <p> {nombre} <span> ${precio} </span> </p>
            <Link to={'/producto/' + id}>
                Más información...
            </Link>
        </li>
    );
}

export default Producto;